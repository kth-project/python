# def power(item):
#     return item * item

# def under_3(item):
#     return item < 3

# list_input_a = [1,2,3,4,5]

# output_a = map(power, list_input_a)
# print("#map() 함수의 실행 결과")
# print("map(power, list_input_a):", output_a)
# print("map(power, list_input_a):", list(output_a))
# print()

# output_b = filter(under_3, list_input_a)
# print("#filter() 함수의 실행 결과")
# print("filter(under_3, list_input_a):", output_b)
# print("filter(under_3, list_input_a):", list(output_b))

# power = lambda x: x * x
# under_3 = lambda x: x < 3 

# list_input_a = [1,2,3,4,5]

# # map () 함수를 사용합니다.
# output_a = map(power, list_input_a)
# print("# map() 함수의 실행 결과")
# print("map(power, list_input_a): ", output_a)
# print("map(power, list_input_a): ", list(output_a))
# print()

# # filter() 함수를 사용합니다.
# output_b = filter(under_3, list_input_a)
# print("# filter() 함수의 실행 결과")
# print("filter(under_3, list_input_a): ", output_b)
# print("filter(under_3, list_input_a): ", list(output_b))

# with open("basic.txt", "r") as file:

#     contents = file.read()

# print(contents)

# 랜덤한 숫자를 만들기 위해 가져옵니다
# import random

# # 간단한 한글 리스트를 만듭니다.
# hanguls = list("가나다라마바사아자차카파타하")

# #파일을 쓰기 모드로 엽니다.
# with open("info.txt", "w") as file:
#     for i in range(1000):
#         #랜덤한 값으로 변수를 생성 합니다.
#         name = random.choice(hanguls) + random.choice(hanguls)
#         weight = random.randrange(40, 100)
#         height = random.randrange(140,200)
#         file.write(f"{name}, {weight}, {height}\n")

# with open("info.txt", "r") as file:
#     for line in file:
#         (name, weight, height) = line.strip().split(",")
#         if (not name) or (not weight) or (not height):
#             continue
#         bmi = int(weight) / ((int(height) / 100) ** 2)
#         result = ""
#         if 25 <= bmi:
#             result = "과체중"
#         elif 18.5 <= bmi:
#             result = "정상 체중"
#         else:
#             result = "저체중"

#         print('\n'.join([
#             "이름: {}",
#             "몸무게: {}",
#             "키: {}",
#             "BMI: {}",
#             "결과: {}"
#         ]).format(name, weight, height, bmi, result))

# def test():
#     print("함수가 호출 됨")
#     yield "test"

# print("A 지점 통과")
# test()

# print("B 지점 통과")
# print(test())

# #함수를 선언 합니다.
# def test():
#     print("A 지점 통화")
#     yield 1
#     print("B 지점 통과")
#     yield 2
#     print("C 지점 통과")
# # 함수를 호출합니다.
# output = test()
# #next() 함수를 호출 합니다.
# print("D 지점 통과")
# a = next(output)
# print(a)
# print("E 지점 통과")
# b = next(output)
# print(b)
# print("F 지점 통과")
# c = next(output)
# print(c)
# # 한 번 더 실행하기
# next(output)

# books = [{
#     "제목": "혼공파",
#     "가격": "18"
# },{
#     "제목": "혼공파 + 딥러닝",
#     "가격": "26"
# },{
#     "제목": "혼공자",
#     "가격": "24"
# }]

# def 가격추출함수(book):
#     return book["가격"]

# print("# 가장 저렴한 책")
# print(min(books, key=가격추출함수))

# print("# 가장 비싼 책")
# print(max(books, key=가격추출함수))

# books = [{
#     "제목" : "혼공파",
#     "가격" : "18"
# },{
#     "제목" : "혼공파 + 딥러닝",
#     "가격" : "26"
# },{
#     "제목" : "혼공자",
#     "가격" : "23"
# }]

# print("# 가장 저렴한 책")
# print(min(books, key=lambda book : book["가격"]))
# print()

# print("# 가장 비싼 책")
# print(max(books, key=lambda book: book["가격"]))

# a = [134,1233,534,6,456,345,3,32,413,6,467,654,7]
# print(a)
# a.sort(reverse=True)
# print(a)

# books = [{
#     "제목" : "혼공파",
#     "가격" : "18"
# },{
#     "제목" : "혼공파 + 딥러닝",
#     "가격" : "99"
# },{
#     "제목" : "혼공자",
#     "가격" : "24"
# }]
# print("# 가격 오름차순 정렬")
# books.sort(key=lambda book: book["가격"])
# for i in books:
#     print(i)

# def object_chane2(f):
#     f = [4,5,6]

# e = [1,2,3]

# print(e)
# object_chane2(e)
# print(e)

# 123

# numbers= [1,2,3,4,5,6]

# print("::".join(str(numbers).strip().split(",")))

# # 변수를 선언 합니다.
# list_input_a = ["123", "234", "43", "스파이", "103"]

# #반복을 적용합니다.
# list_number= []
# for item in list_input_a:
#     try:
#         float(item)
#         list_number.append(item)
#     except:
#         pass

# #출력합니다
# print(f"{list_input_a} 내부에 있는 숫자는")
# print(f"{list_number} 입니다.")

# #try execpt 구문으로 예외를 처리합니다.
# try:
#     # 숫지로 변환 합니다.
#     number_input_a = int(input("정수 입력 > : "))
#     #출력 합니다.
#     print("원의 반지름:", number_input_a)
#     print("원의 둘레: ", 2 * 3.14 * number_input_a)
#     print("원의 넓이:", 3.14 * number_input_a * number_input_a)
# except:
#     print("정수 입력 안함")
# else:
#     print("예외가 발생하지 않음")
# finally:
#     print("일단 프로그램이 어떻게든 끝났습니다.")

# #함수를 선언 합니다.
# def write_text_file(filename, text):
#     # try execpt 구문을 사용합니다.
#     try:
#         #파일을 엽니다.
#         file = open(filename, "w")
#         file.write(text)
#         #여러 가지 처리를 수행 합니다.
#         return
#         # file.write(text)
#     except Exception as e :
#         print(e)
#     finally:
#         file.close()

# write_text_file("kth.txt", "안녕하노")

# print("프로그램이 시작되었습니다.")

# while True:
#     try:
#         print("try 구문이 실행 되었습니다.")
#         break
#         print("try 구문이 break 키워드 뒤입니다.")
#     except:
#         print("execpt 구문이 실행 되었습니다.")
#     finally:
#         print("finally 구문이 실행 되었습니다.")
#     print("whule 반복문의 마지막 줄입니다.")
# print("프로그램이 종료 되었습니다.")

# try execpt 구문으로 예외를 처리합니다.
# try:
#     #숫자로 변환 합니다.
#     number_input_a = int(input("정수 입력>"))
#     # 출력합니다.
#     print("원의 반지름:", number_input_a)
#     print("원의 둘레:", 2 * 3.14 * number_input_a)
#     print("원의 넓이:", 3.14 * number_input_a * number_input_a)
# except Exception as e:
#     #예외 객체를 출력해봅니다.
#     print("type:", type(e))
#     print("e:", e)

# #변수를 선언 합니다.
# list_number = [234,534245,123,4352,546,3,45]

# #try except 구문으로 예외를 처리합니다.
# try:
#     #숫자를 입력 받습니다.
#     number_input = int(input("정수 입력> "))
#     #리스트의 요소를 출력합니다.
#     print(f"{number_input}번째 요소: {list_number[number_input]}")
# except ValueError:
#     print("정수 입력하세용~")
# except IndexError:
#     print("리스트의 인덱스를 벗어났어용")

# list_number = [52,234,456,567,3451,123234,3454,264536,23]

# try:
#     number_input = int(input("정수 입력>"))
#     print(f"{number_input}번쨰 인덱스의 요소는{list_number[number_input]} 입니둥 ~ ")
#     # 예욍를발생()
# except ValueError as exception:
#     print("정수를 입력해주세용")
#     print(type(exception), exception)
# except IndexError as exception:
#     print("리스트의 인덱스를 벗어났어 융 ~")
#     print(type(exception),exception)
# except Exception as exception:
#     print("예상치 못한 오류 발생")
#     print(type(exception), exception)

# number = input("정수 입력 > ")
# number = int(number)

# if number > 0 :
#     raise NotImplementedError
# else:
#     raise NotImplementedError

# import math

# print(math.sin(1))

# from math import sin, cos, tan, floor, ceil

# print(sin(1))
# print(cos(1))
# print(tan(1))
# print(floor(2.5))
# print(ceil(2.5))

# import math as m 

# print(m.sin(1))
# print(m.cos(1))
# print(m.tan(1))
# print(m.floor(2.5))
# print(m.ceil(2.5))

# import random
# print("# 랜덤 모듈")

# # random(): 0.0 <= x < 1.0 사이의 float를 리턴합니다.
# print(" - random(): ", random.random())

# #uniform(min,max): 지정한 범위 사이의 float를 리턴합니다.
# print(" - unfirom(10,20):", random.uniform(10,20))

# #randrange(): 지정한 범위의 int를 리턴합니다.
# # - randrange(max): 0부터 max 값을 리턴합니다.
# # - randrange(min, max): min부터 max 사이의 값을 리턴합니둥.
# print(" - randrang(10):", random.randrange(100))

# #choic(list) : 리스트 내부에 있는 요소를 랜덤하게 선택함둥
# print(" - choic([1,2,3,4,5]):", random.choice([1,2,3,4,5]))

# #shuffle(list) : 리스트의 요소들을 랜덤하게 섞습니다.
# print(" - shuffle([1,2,3,4,5]):", random.shuffle([1,2,3,4,5]))

# # sample(list, k=<숫자>): 리스트의 요소 중에 k개를 뽑습니다.
# print(" - sample([1,2,3,4,5], k =2):", random.sample([1,2,3,4,5], k=3))

# #sys 모듈
# #모듈을 읽어 드립니다.
# import sys

# print(sys.argv)
# print("---")

# #컴퓨터 환경과 관련된 정보를 출력합니다.
# print("getwindowsversion:()", sys.getrecursionlimit)
# print("---")
# print("copyright:", sys.copyright)
# print("---")
# print("version:", sys.version)

# sys.exit()

# #모듈을 읽어 드립니다.
# import os

# #기본 정보를 몇개 출력해 봅시둥.
# print("현재 운영체제: ", os.name)
# print("현재 폴더:", os.getcwd())
# print("현재 폴더 내부의 요소:", os.listdir())

# #폴더를 만들고 제거합니다.()
# os.mkdir("hello")
# os.rmdir("hello")

# #파일을 생성하고 + 파일 이름을 변경합니다.
# with open("original.txt", "w") as file:
#     file.write("hello")
# os.rename("original.txt", "new.txt")

# #파일을 제거합니다.
# os.remove("new.txt")

# #시스템 명령어 실행
# os.system("pwd")

# import datetime

# print("#현재 시각 출력하기")
# now = datetime.datetime.now()
# print(now.year, "년")
# print(now.month, "월")
# print(now.day, "일")
# print(now.hour,"시")
# print(now.minute, "분")
# print(now.second, "초")
# print()

# print("# 시간을 포맷에 맞춰 출력하기")
# output_a = now.strftime("%Y.%m.%d %H:%M:%S")
# output_b = "{}년 {}월 {}일 {}시 {}분 {}초".format(now.year,\
#     now.month,\
#     now.day,\
#     now.hour,\
#     now.minute,\
#     now.second)
# output_c = now.strftime("%Y{} %m{} %d{} %H{} %M{} %S{}").format(*"년월일시분초")
# print(output_a)
# print(output_b)
# print(output_c)


# #모듈을 읽어 드립니다.
# import datetime
# now = datetime.datetime.now()

# #특정 시간 이후의 시간 구하기
# print("# datetime.timedelta로 시간 더하기")
# after = now + datetime.timedelta(\
#     weeks=1,\
#     days=1,\
#     hours=1,\
#     minutes=1,\
#     seconds=1)
# print(after.strftime("%Y{} %m{} %d{} %H{} %M{} %S{}").format(*"년월일시분초"))

# #특정 시간 요소 교체하기
# print("#now.replace()로 1년 더하기")
# output = now.replace(year=(now.year + 1 ))
# print(output.strftime("%Y{} %m{} %d{} %H{} %M{} %S{}").format(*"년월일시분초"))

# import time
# print("지금부터 5초 동안 정지")
# time.sleep(5)
# print("프로그램을 종료 합니다.")

# #모듈을 읽어 드립니다.
# from urllib import request

# # urlopen() 함수로 구글의 메인 페이지를 읽습니다.
# target = request.urlopen("https://google.com")
# output = target.read()

# print(output)

# from operator import itemgetter

# books = [{
#     "제목" : "혼공파",
#     "가격" : 12
# },{
#     "제목" : "혼공자",
#     "가격" : 123
# }]

# print(min(books, key=itemgetter("가격")))
# print(max(books, key=itemgetter("가격")))

# #모듈을 읽어드립니다.
# from urllib import request
# from bs4 import BeautifulSoup

# #urlopen() 함수로 기상청의 전국 날씨를 읽습니다.
# target = request.urlopen("http://www.kma.go.kr/weather/forecast/mid-term-rss3.jsp?stnId=108")

# #BeautfulSoup을 사용해 웹페이지를 분석합니다.
# soup = BeautifulSoup(target, "html.parser")

# #location 태그를 찾습니다.
# for location in soup.select("location"):
#     # 내부 city, wf, tmn, tmx 태그를 찾아 출력합니다.
#     print("도시:", location.select_one("city").string)
#     print("날씨:", location.select_one("wf").string)
#     print("최저기온:", location.select_one("tmn").string)
#     print("최고기온:", location.select_one("tmx").string)
#     print()

# from flask import Flask
# app = Flask(__name__)

# @app.route("/")
# def hello():
#     return "<h1>Hello World!</h1>"

#모듈을 읽어 들입니다.

# from flask import Flask
# from urllib import request
# from bs4 import BeautifulSoup

# app = Flask(__name__)
# @app.route("/")

# def hello():
#     target = request.urlopen("http://www.kma.go.kr/weather/forecast/mid-term-rss3.jsp?stnId=108")

#     soup = BeautifulSoup(target, "html.parser")

#     output = ""
#     for location in soup.select("location"):
#         output += "<h3>{}</h3>".format(location.select_one("city").string)
#         output += "날씨: {}<br/>".format(location.select_one("wf").string)
#         output += "최저/최고 기온: {}/{}"\
#             .format(\
#                 location.select_one("tmn").string,\
#                 location.select_one("tmx").string\
#             )
        
#         output += "<hr/>"
#     return output

# def test(function):
#     def wrapper():
#         print("인사가 시작되었습니다.")
#         function()
#         print("인사가 종료되었습니다.")
#     return wrapper


# @test
# def hello():
#     print("hello")

# hello()

# from functools import wraps

# def test(function):
#     @wraps(function)
#     def wrapper(*arg, **kwargs):
#         print("인사가 시작되었습니다.")
#         function(*arg, **kwargs)
#         print("인사가 종료되었습니다.")
#     return wrapper

# test("안녕")

# #모듈을 읽어 들입니다.
# from urllib import request

# #urlopen() 함수로 구글의 메인페이지를 읽습니다.
# target = request.urlopen("https://www.hanbit.co.kr/images/common/logo_hanbit.png")
# output = target.read()
# print(output)

# #write binary[바이너리 쓰기] 모드로
# file = open("output.png", "w")
# file.write(output)
# file.close()

# #클래스를 선언합니다.
# class student:
#     def __init__(self, name, koren, math, english, science):
#         self.name = name
#         self.koren = koren
#         self.math = math
#         self.english = english
#         self.science = science

# #학생 리스트를 선언합니다.
# student = [
#     Student("신재원", 23,35,57,56),
#     Student("양지원", 45,67,45,23),
#     Student("이동건",45,72,78,45),
#     Student("노병화",36,77,89,99),
#     Student("손준혁",00,00,00,00)
# ]

# #Student 인스턴스의 속성에 접근 방법
# student[0].name

# class Student:
#     def __init__(self):
#         pass

# student = Student()

# print("isinstance(student, Student)", isinstance(student, Student))

# class Car():
#     def __init__(self, _company, _details):
#         self._company = _company
#         self._details = _details
    
#     def __str__(self):
#         return f'str : {self._company} - {self._details}'
    
#     def __repr__(self):
#         return f'repr : {self._company} - {self._details}'
    
#     def __reduce__(self):
#         pass
    
# car1 = Car('Ferrari', {'color':'white', 'horsepower':400, 'price': 8000})
# car2 = Car('Bmw', {'color':'block', 'horsepower':200, 'price': 3000})
# car3 = Car('Audi', {'color':'green', 'horsepower':300, 'price': 1000})

# print(car1)
# print(car2)
# print(car3)

# print(car1.__dict__)
# print(car2.__dict__)
# print(car3.__dict__)

# #print(dir(car1))

# print()
# print()

# #리스트 선언
# car_list = []

# car_list.append(car1)
# car_list.append(car2)
# car_list.append(car3)

# print(car_list)

# print()
# print()

# for x in car_list:
#     print(x)


# class Car():
#     '''
#     car class
#     Author : Kim
#     Date :2023.07.4

#     '''

#     car_count = 0


#     def __init__(self, company, details):
#         self._company = company
#         self._details = details
#         Car.car_count += 1
    
#     def __str__(self):
#         return f'str : {self._company} - {self._details}'
    
#     def __repr__(self):
#         return f'repr : {self._company} - {self._details}'
    
#     def __del__(self):
#         Car.car_count -= 1
    
#     def detail_info(self):
#         print(f"Current ID {id(self)}")
#         print(f"Car Detail Info : {self._company} {self._details.get('price')}")
        
        

# #Self 의미
# car1 = Car('Ferrari', {'color':'white', 'horsepower':400, 'price': 8000})
# car2 = Car('Bmw', {'color':'block', 'horsepower':200, 'price': 3000})
# car3 = Car('Audi', {'color':'green', 'horsepower':300, 'price': 1000})

# # ID 확인
# print(id(car1))
# print(id(car2))
# print(id(car3))

# print(car1._company == car2._company)
# print(car1 is car2)

# # dir & __dict__ 확인
# print(dir(car1))
# print(dir(car2))

# print()
# print()

# print(car1.__dict__)
# print(car2.__dict__)


# #Doctirg
# print(Car.__doc__)

# # 실행
# car1.detail_info()
# Car.detail_info(car1)
# car2.detail_info()
# Car.detail_info(car2)


# # 비교
# print(car1.__class__ == car2.__class__)
# print(id(car1.__class__), id(car2.__class__))

# # 에러
# #Car.detail_info()

# #공유 확인
# print(car1.car_count)
# print(car2.car_count)
# print(car1.__dict__)
# print(car2.__dict__)
# print(dir(car1))

# # 접근
# print(car1.car_count)
# print(Car.car_count)

# del car2
# # 삭제 확인
# print(car1.car_count)
# print(Car.car_count)

# # 인스턴스 네임스페이스에 없으면 상위에서 검색
# # 즉, 동일한 이름으로 변수 생성 가능(인스턴스 검색 후 -> 상위(클래스 변수, 부모클래스 변수))

# class Car():
#     '''
#     car class
#     Author : Kim
#     Date :2023.07.4
#     Description : Class, static, Instance Nethod
#     '''

#     # 클래스 변수(모든 인스턴스가 공유)
#     price_per_raise = 1.0

#     def __init__(self, company, details):
#         self._company = company
#         self._details = details

#     def __str__(self):
#         return f'str : {self._company} - {self._details}'
    
#     def __repr__(self):
#         return f'repr : {self._company} - {self._details}'
    
#     #Instance Method
#     #self : 객체의 고유한 속성 값을 사용
#     def detail_info(self):
#         print(f"Current ID {id(self)}")
#         print(f"Car Detail Info : {self._company} {self._details.get('price')}")
        
#     # Instance Method
#     def get_price(self):
#         return f'before car Price -> commpany : {self._company}, price : {self._details.get("price")}'

#     # Instance Method
#     def get_price_culc(self):
#         return f'after car Price -> commpany : {self._company}, price : {self._details.get("price") * Car.price_per_raise}'

#     # Class Method
#     @classmethod
#     def raise_price(cls, per):
#         if per <= 1:
#             print("Please Enter 1 Or More")
#         cls.price_per_raise = per
#         print('Succeed! price increased')

#     # Static Method
#     @staticmethod
#     def is_bmw(inst):
#         if inst._company == 'Bmw':
#             return f'OK! This car is {inst._company}'
#         return 'sorry. This car is not Bmw'

# #Self 의미
# car1 = Car('Ferrari', {'color':'white', 'horsepower':400, 'price': 8000})
# car2 = Car('Bmw', {'color':'block', 'horsepower':200, 'price': 3000})

# # 전체 정보
# car1.detail_info()
# car2.detail_info()

# # 가격 정보(직접 접근)
# print(car1._details.get('price'))
# print(car2._details.get('price'))

# # 가격정보(인상 전)
# print(car1.get_price())
# print(car2.get_price())

# #가격 인상(클래스메소드 미사용)
# Car.price_per_raise = 1.04

# #가격정보(인상 후)
# print(car1.get_price_culc())
# print(car2.get_price_culc())

# #가격 인상(클래스메소드 사용)
# Car.raise_price(2)

# #가격정보(인상 후)
# print(car1.get_price_culc())
# print(car2.get_price_culc())

# # 인스턴스로 호출(스테이틱)
# print(car1.is_bmw(car1))
# print(car2.is_bmw(car2))
# # 클래스로 호출(스테이틱)
# print(Car.is_bmw(car1))
# print(Car.is_bmw(car2))

# Special Method(Magic Method)
# 파이썬의 핵심 -> 시퀀스(Sequence), 반복(iteragtor), 함수(Functions), Class(클래스)
# 클래스안에 정의할 수 있는 특별한(Built-in) 메소드

# #기본형
# print(int)
# print(float)

# # 모든 속성 및 메소드 출력
# print(dir(int))
# print(dir(float))

# n = 10

# print(n + 100)
# print(n.__add__(100))
# # print(n.__doc__)
# print(n.__bool__(), bool(n))
# print(n * 100, n.__mul__(100))

# print()
# print()


# # 클래스 예제1
# class Fruit:
#     #초기화 메서드
#     def __init__(self, name,price):
#         #인스턴스 변수
#         self._name = name
#         self._price = price

#     # Str 메서드 
#     def __str__(self):
#         return f'Fruit Class Info : {self._name}, {self._price}'
    
#     # 더하기 메서드
#     def __add__(self, x):
#         print("Called >> __add__")
#         return self._price + x._price
    
#     # 빼기 메서드
#     def __sub__(self, x):
#         print("called >> __sub__")
#         return self._price - x._price
    
#     def __le__(self,x):
#         print("Called >> __le__")
#         if self._price <= x._price:
#             return True
#         else:
#             return False

#     def __ge__(self,x):
#         print("Called >> __ge__")
#         if self._price >= x._price:
#             return True
#         else:
#             return False


# #인스턴스 생성
# s1 = Fruit("Orage", 7500)
# s2 = Fruit("Banana", 3000)

# print(s1 + s2)
# print(s1 - s2)

# # 일반적인 계산
# # print(s1._price + s3._price)

# #매직메소드
# print(s1 >= s2)
# print(s1 <= s2)
# print(s1 - s2)
# print(s2 - s1)
# print(s1)
# print(s2)



# 클래스 예제2
#(5,2) + (4,3) = (9,5)
#(10,3) * 5 = (50,15)
#Max((5,10)) = 10

# class Vector():
#     def __init__(self, *args):
#         '''
#         Create a vector, example : v = Vector(5.10)
#         '''
#         if len(args) == 0:
#             self._x, self._y = 0,0
#         else:
#             self._x, self._y = args

#     def __repr__(self):
#         '''
#         Return the vector infomations.
#         '''
#         return 'Vector(%r, %r)' %(self._x,self._y)
    
#     def __add__(self,other):
#         '''
#         Return the vector addtion of self and other
#         '''
#         return Vector(self._x + other._x, self._y + other._y)

#     def __mul__(self,y):
#         return Vector(self._x * y, self._y * y)
    
#     def __bool__(self):
#         return bool(max(self._x,self._y))


# #vector 인스턴스 생성
# v1 = Vector(5,7)
# v2 = Vector(23,35)
# v3 = Vector()

# #매직메소드 출력
# print(Vector.__init__.__doc__)
# print(Vector.__add__.__doc__)
# print(Vector.__repr__.__doc__)
# print(v1, v2, v3)
# print(v1 + v2)
# print(v1 * 3)
# print(v1 * 10)
# print(bool(v1), bool(v2), bool(v3))

#객체 -> 파이썬의 데이터를 추상화
# 모든 객체 -> id, type -> value

# # 일반적인 튜플
# pt1 = (1.0, 5.0)
# pt2 = (2.5, 1.5)

# from math import sqrt

# l_leng1 = sqrt((pt1[0] - pt2[0]) ** 2 + (pt1[1] - pt2[1]) ** 2)

# print(l_leng1)

# #네임드 튜플 선언
# from collections import namedtuple

# #네임드 튜플 선언
# Point = namedtuple("Point", "x y")

# pt3 = Point(1.0,5.0)
# pt4 = Point(2.5,1.5)

# # print(pt3)
# # print(pt4)

# l_leng2 = sqrt((pt3.x - pt4.x) ** 2 + (pt3.y - pt4.y) ** 2)

# print(l_leng2)

# # 네임드 튜플 선언 방법
# Point1 = namedtuple("Point", ["x", "y"])
# Point2 = namedtuple("Point", "x, y")
# Point3 = namedtuple("Point", "x y")
# Point4 = namedtuple("Point", "x y x class", rename=True) # Default = False

# #출력
# print(Point1, Point2, Point3, Point4)

# # Dict to unpacking
# temp_dict = {"x" : 75, "y": 55}

# # 객체 생성
# p1 = Point1(x=10, y=35)
# p2 = Point2(20,40)
# p3 = Point(45, y=20)
# p4 = Point4(10, 20, 30, 40)
# p5 = Point(**temp_dict)

# print()

# print(p1)
# print(p2)
# print(p3)
# # rename 테스트
# print(p4)
# print(p5)


# #사용
# print(p1[0] + p2[1])
# print(p1.x + p2.y)

# #unpacking
# x ,y =p3
# print(x,y)

# 실 사용 실습 
# 반20명, 4개의 반(A,B,C,D)

# from collections import namedtuple

# Classes = namedtuple('classes', ['rank', 'number'])

# # 그룹 리스트 선언
# numbers = [str(n) for n in range(1,21)]
# ranks = 'a b c d'.split()

# print(numbers)
# print(ranks)

# # List comprehension
# students = [Classes(rank, number) for rank in ranks for number in numbers]

# print(len(students))
# print(students)

# students2 = [Classes(rank,number)
#              for rank in 'A B C D'.split()
#                 for number in [str(n)
#                     for n in range(1,21)]]

# print(students2)

# for s in students2:
#     print(s)




